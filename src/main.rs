use ggez::graphics::Color;
use ggez::*;

const ROCKET_HEIGHT:f32 = 100.0;
const ROCKET_WIDTH:f32 = 20.0;
const ROCKET_HEIGHT_HALF:f32 = ROCKET_HEIGHT * 0.5;
const ROCKET_WIDTH_HALF:f32 = ROCKET_WIDTH * 0.5;

pub fn main() {
    let state = State {
        dt: std::time::Duration::new(0, 0),
        time_left: 0.0,
    };

    let c = conf::Conf::new();
    let (ctx, event_loop) = ContextBuilder::new("Hello_ggez", "awesome_person")
        .default_conf(c)
        .build()
        .unwrap();

    event::run(ctx, event_loop, state);
}

struct State {
    dt: std::time::Duration,
    time_left: f32,
}

impl ggez::event::EventHandler<GameError> for State {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        self.dt = timer::delta(ctx);
        self.time_left += self.dt.as_secs_f32();
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, Color::BLACK);
        let rect = graphics::Rect::new(10.0, 10.0, 300.0, 150.0);
        let rect_mesh = graphics::Mesh::new_rectangle(
            ctx,
            graphics::DrawMode::fill(),
            rect,
            Color::new(1.0, 1.0, 1.0, 1.0),
        )?;

        graphics::draw(ctx, &rect_mesh, graphics::DrawParam::default());

        graphics::present(ctx);

        if self.time_left > 2.0 {
            self.time_left = 0.0;
            println!("Hello ggez! dt = {}ms", self.dt.as_millis());
        }
        Ok(())
    }
}
